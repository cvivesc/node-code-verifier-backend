import { Delete, Get, Post, Put, Query, Route, Tags } from "tsoa";
import { IUsersController } from "./interfaces";
import { LogSuccess, LogError, LogWarning } from "../utils/logger";

// ORM
import { deleteUserByID, getAllUsers, getKatasFromUser, getUserByID, updateUserByID } from "../domain/orm/User.orm";
import { BasicResponse } from "./types";

@Route("/api/users")
@Tags("UsersController")
export class UsersController implements IUsersController {
        
    /**
     * End point to retrieve the Users in the Collection "Users" of DB
     * @param {string} id Id of user to retreive (optional)
     * @returns All users or user found by ID
     */
    @Get("/")
    public async getUsers(@Query()page:number, @Query()limit: number, @Query()id?: string): Promise<any> {
        let response: any = '';
        if (id) {
            LogSuccess(`[/api/user] Get User By ID ${id}`);
            
            response = await getUserByID(id);
            return response;
        }else{
            LogSuccess('[/api/users] Get all Users Request')
            
            response = await getAllUsers(page, limit);
            return response;
        }
    }
    
    /**
     * End point to delete the User in the Collection "Users" of DB
     * @param {string} id Id of user to delete (optional) 
     * @returns message informing if deletion was correct
     */
    @Delete("/")
    public async deleteUser(@Query()id?: string): Promise<any> {
        let response: any = '';
        if (id) {
            LogSuccess(`[/api/user] Delete User By ID ${id}`);
            
            await deleteUserByID(id).then((r) => {
                response = {
                    status: 200,
                    message: `User with id: ${id} deleted succesfully`
                }
            })
            
            //response = await deleteUserByID(id);
            return response;
        }else{
            LogWarning('[/api/users] Delete User Request WHITOUT ID')
            
            response = {
                status: 400,
                message: 'Please, provide an ID to remove user from Database'
            }
            return response;
        }
    }
    
    // @Post("/")
    // public async createUser(user: any): Promise<any> {
    //     let response: any = '';
        
    //     await createUser(user).then((r) => {
    //         LogSuccess(`[/api/user] User created ${user}`);
    //         response = {
    //             message: `User created successfully: ${user.name}`
    //         }
    //     })
    //     return response;
    // }
    
    @Put("/")
    public async updateUser(@Query()id: string, user: any): Promise<any> {
        let response: any = '';

        if (id) {
            LogSuccess(`[/api/user] Update User By ID ${id}`);
            
            await updateUserByID(id, user).then((r) => {
                response = {
                    status: 200,
                    message: `User with id: ${id} updated succesfully`
                }
            })
            
            //response = await deleteUserByID(id);
            return response;
        }else{
            LogWarning('[/api/users] Update User Request WHITOUT ID')
            
            response = {
                status: 400,
                message: 'Please, provide an ID to update user'
            }
            return response;
        }
    }

    @Get("/katas")
    public async getKatas(@Query()page:number, @Query()limit: number, @Query()id: string): Promise<any> {
        let response: any = '';
        if (id) {
            LogSuccess(`[/api/user/katas] Get katas from user By ID ${id}`);
            
            response = await getKatasFromUser(page, limit, id);
        }else{
            LogSuccess('[/api/users/katas] Get katas without user id');
            response = {
                message: 'Id from user is needed'
            }
        }
        return response;
    }
    
}