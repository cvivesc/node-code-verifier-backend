import { BasicResponse, GoodbyeResponse } from "../types";
import { IUser } from "../../domain/interfaces/IUser.interface"
import { IKata } from "../../domain/interfaces/IKata.interface";

export interface IHelloController {
    getMessage(name?:string): Promise<BasicResponse>
}

export interface IGoodbyeController {
    getMessage(name?:string): Promise<GoodbyeResponse>
}

export interface IUsersController {
    // Read all Users from database || Find user by ID
    getUsers(page:number, limit: number, id?: string): Promise<any>
    // Get all Katas of a User
    getKatas(page:number, limit: number, id?: string): Promise<any>
    // Delete User by ID from database
    deleteUser(id?: string): Promise<any>
    // Create new User
    //createUser(user: any): Promise<any>
    // Update User by ID
    updateUser(id: string, user: any): Promise<any>
}

export interface IAuthController {
    // Register users
    registerUser(user: IUser): Promise<any>
    //login user
    loginUser(auth: any): Promise<any>
}

export interface IKatasController {
    // Read all Users from database || Find user by ID
    getKatas(page:number, limit: number, id?: string,level?: string): Promise<any>
    // Delete User by ID from database
    deleteKata(id?: string): Promise<any>
    // Create new Kata
    createKata(kata: IKata): Promise<any>
    // Update Kata by ID
    updateKata(id: string, kata: IKata): Promise<any>
}