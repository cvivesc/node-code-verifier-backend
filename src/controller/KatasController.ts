import { Delete, Get, Post, Put, Query, Route, Tags } from "tsoa";
import { IKatasController } from "./interfaces";
import { LogSuccess, LogError, LogWarning } from "../utils/logger";

// ORM
import { getAllKatas, getKataByID, createKata, getKataByLevel, deleteKataByID, getKatasLast5, getKatasTop, getKatasChances, updateKataValoration, updateKataByID } from "../domain/orm/Kata.orm";
import { BasicResponse } from "./types";
import { IKata } from "../domain/interfaces/IKata.interface";


@Route("/api/katas")
@Tags("KatasController")
export class KatasController implements IKatasController {
    
    /**
     * End point to retrieve the Katass in the Collection "Katas" of DB
     * @param {string} id Id of kata to retreive (optional)
     * @param {string} level Level of katas to retreive (optional)
     * @returns All katas or katas found by ID or LEVEL
     */
    @Get("/")
    public async getKatas(@Query()page: number, @Query()limit: number, @Query()id?: string, level?: string): Promise<any> {
        let response: any = '';
        if (id) {
            LogSuccess(`[/api/katas] Get Kata By ID ${id}`);
            
            response = await getKataByID(id);
            return response;
        }else if (level){
            LogSuccess(`[/api/katas] Get Katas By LEVEL ${level}`);
            
            response = await getKataByLevel(level);
            return response;
        }else{
            LogSuccess('[/api/katas] Get all Katass Request')
            
            response = await getAllKatas(page, limit);
            return response;
        }    
    };

    @Get("/5last")
    public async getKatasLast5(): Promise<any> {
        let response: any = '';

            response = await getKatasLast5();
            return response;
    };
    
    @Get("/top")
    public async getKatasTop(): Promise<any> {
        let response: any = '';

            response = await getKatasTop();
            return response;
    };

    @Get("/chances")
    public async getKatasChances(): Promise<any> {
        let response: any = '';

            response = await getKatasChances();
            return response;
    };



    /**
     * End point to delete the Kata in the Collection "katass" of DB
     * @param {string} id Id of kata to delete (optional) 
     * @returns message informing if deletion was correct
     */
    @Delete("/")
    public async deleteKata(@Query()id?: string,@Post()userID?:string): Promise<any> {
        let response: any = '';
        if (id && userID) {
            LogSuccess(`[/api/katas] Delete Kata By ID ${id}`);
            
            await deleteKataByID(id, userID).then((r) => {
                response = {
                    messaje: `Kata with id: ${id} deleted succesfully`
                }
            })
            
            //response = await deleteUserByID(id);
            return response;
        }else{
            LogWarning('[/api/katas] Delete Kata Request WHITOUT ID')
            
            response = {
                message: 'Please, provide an ID to remove kata from Database'
            }
            return response;
        }
    };
    
    @Post("/")
    public async createKata(kata: IKata): Promise<any> {
        let response: any = '';
        
        await createKata(kata).then((r) => {
            LogSuccess(`[/api/kata] Kata created ${kata}`);
            response = {
                message: `Kata created successfully: ${kata.name}`
            }
        })
        return response;
    }
    
    @Put("/")
    public async updateKata(@Query()id: string, kata: IKata): Promise<any> {
        let response: any = '';

        if (id) {
            LogSuccess(`[/api/kata] Update kata By ID ${id}`);
            
            await updateKataByID(id, kata).then((r) => {
                response = {
                    status: 200,
                    message: `Kata with id: ${id} updated succesfully`
                }
            })
            
            return response;
        }else{
            LogWarning('[/api/katas] Update kata Request WHITOUT ID')
            
            response = {
                status: 400,
                message: 'Please, provide an ID to update kata'
            }
            return response;
        }
    }

    @Put("/")
    public async updateKataValoration(@Query()id: string, valoration: any): Promise<any> {
        let response: any = '';

        if (id) {
            LogSuccess(`[/api/katas] Update Kata valoration By ID ${id}`);
            
            await updateKataValoration(id, valoration).then((r) => {
                response = {
                    messaje: `Kata with id: ${id} updated succesfully`
                }
            })
            return response;
        }else{
            LogWarning('[/api/katas] Update Kata Request WHITOUT ID')
            
            response = {
                message: 'Please, provide an ID to update kata'
            }
            return response;
        }
    }
    
}