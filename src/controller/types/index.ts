/**
 * Basic JSON response for Controllers
 */
export type BasicResponse = {
    message: string
}

/**
 * Error JSON response for Controllers
 */
export type ErrorResponse = {
    error: string,
    message: string
}

/**
 * Goodbye JSON response for Controllers
 */
export type GoodbyeResponse = {
    message: string,
    date: Date
}

/**
 * Auth JSON response for Controllers
 */
 export type AuthResponse = {
    token: string,
    message: string,
    status: number
}
