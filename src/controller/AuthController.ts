import { Delete, Get, Post, Put, Query, Route, Tags } from "tsoa";
import { IAuthController } from './interfaces';
import { LogSuccess, LogError, LogWarning } from "../utils/logger";
import { IUser } from "../domain/interfaces/IUser.interface";
import { IAuth } from "../domain/interfaces/IAuth.interface";

import { registerUser, loginUser, logoutUser, getUserByID } from "../domain/orm/User.orm";
import { AuthResponse } from "./types";

@Route("/api/auth")
@Tags("AuthController")
export class AuthController implements IAuthController {

    @Post("/register")
    public async registerUser(user: IUser): Promise<any> {
        
        let response: any = '';
        if (user) {
            LogSuccess(`[/api/auth/register] Register new User: ${user.name}`);
            await registerUser(user).then((r) => {
                LogSuccess(`[/api/auth/register] User created ${user.name}`);
                response = {
                    status: 201,
                    message: `User created successfully: ${user.name}`
                }
            })
        }else{
            LogWarning('[/api/auth/register] Register needs User Entity')
            response = {
                status: 400,
                message: 'User not Registered: Please, provide a User Entity to create one'
            }
        }
        return response;
    }

    @Post("/login")
    public async loginUser(auth: IAuth): Promise<any> {

        let response: AuthResponse | undefined;

        if(auth){
            LogSuccess(`[/api/auth/login] Trying to login User: ${auth.email}`);
            let data = await loginUser(auth);
            response = {
                token: data.token,
                message: `Welcome, ${data.user.name}`,
                status: 200
            }
            // await loginUser(auth).then((r) => {
            //     LogSuccess(`[/api/auth/login] Logged in User: ${auth.email}`);
            //     response = {
            //         token: r.token, // JWT generated for logged in user
            //         message: `User Logged In Successfully : ${auth.email}`,
            //         status: 200
            //     }
            // }) 
        }else{
            LogWarning('[/api/auth/login] Login needs Auth Entity (email && password)');
            response = {
                token: 'NOT VALID',
                message: 'Please, provide an email && password to login',
                status: 400
            }
        }

        return response;
    }

    /**
     * End point to retrieve the User in the Collection "Users" of DB
     * Middleware: Validate JWT
     * In Headers you must add the x-access-token with a valid JWT
     * @param {string} id Id of user to retreive (optional)
     * @returns User found by ID
     */
    @Get("/me")
    public async userData(@Query()id: string): Promise<any> {
        let response: any = '';
        if (id) {
            LogSuccess(`[/api/user] Get User Data By ID ${id}`);
            
            response = await getUserByID(id);
            // Remove the password
            //response.password = '';
        }
        return response;
    };

    @Post("/logout")
    public async logoutUser(): Promise<any> {
        let response: any = '';
        // TODO: Close Session of user
        throw new Error("Method not implemented.");
    }

}