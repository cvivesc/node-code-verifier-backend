import { BasicResponse } from "../controller/types";
import { IKata, KataLevel } from "../domain/interfaces/IKata.interface";
import express, { Request, Response } from "express";
import { dangerouslyDisableDefaultSrc } from "helmet/dist/types/middlewares/content-security-policy";
import { KatasController } from "../controller/KatasController";
import { LogInfo } from "../utils/logger";
import { verifyToken } from "../middlewares/verifyToken.middleware";

// Router from express
let kataRouter = express.Router();

// GET -> http://localhost:8000/api/katas?id=63fcfcd8260f9bb3ae348c1e
kataRouter.route('/')
    // GET:
    .get(async (req: Request, res: Response) => {
        // Obtain a Query Param
        let id: any = req?.query?.id;
        let level: any = req?.query?.level;
        // Pagination
        let page: any = req?.query?.page || 1;
        let limit: any = req?.query?.limit || 10;
        
        LogInfo(`Query Param: id:${id} | level:${level}`);
        // Controller Instance to execute method
        const controller: KatasController = new KatasController();
        // Obtain Response
        const response: any = await controller.getKatas(page, limit, id, level);
        // Send to the client the response
        return res.send(response);

    })
    // DELETE:
    .delete(verifyToken,async (req: Request, res: Response) => {
        // Obtain a Query Param
        let id: any = req?.query?.id;
        let userID: string = req?.body?.userID;
        LogInfo(`Query Param: ${id}`);
        // Controller Instance to execute method
        const controller: KatasController = new KatasController();
        // Obtain Response
        const response: any = await controller.deleteKata(id,userID);
        // Send to the client the response
        return res.send(response);

    })
    // POST:
    .post(verifyToken,async (req: Request, res: Response) => {
        // creating a kata in a object
        // Obtain ID by Query Param
        //let id: any = req?.query?.id;

        // Obtaining data by Body
        let name: any = req?.body?.name || "Default name";
        let description: any = req?.body?.description || "Default description";
        let level: KataLevel = req?.body?.level || KataLevel.BASIC;
        let user: string = req?.body?.user || "";
        let dateString: any = req?.body?.date;
        let dateObj: Date = new Date(dateString ?? new Date());
        let valoration: number = req?.body?.valoration || 0;
        let numvaloration: any = req?.body?.numvaloration || 0;
        let chances: number = req?.body?.chances || 0;
        let solution: string = req?.body?.solution || "";
        let participants: string[] = req?.body?.participants || [];

        if(name && user){
            let newKata: IKata = {
                name: name,
                description: description || "Default description",
                level: level || "Basic",
                user: user,
                date: dateObj,
                valoration: valoration,
                numvaloration: numvaloration,
                chances: chances,
                solution: solution,
                participants: participants,
            }
            // Controller Instance to execute method
            const controller: KatasController = new KatasController();
            // Obtain Response
            const response: any = await controller.createKata(newKata);
            // Send to the client the response
            return res.status(200).send(response);
        }else{
            return res.status(400).send({
                message:'[ERROR] Creating Kata, you ned to send name and user',
            });
        }

    })
    // PUT:
    .put(verifyToken,async (req: Request, res: Response) => {
        // update a kata in a object
        // Obtain ID by Query Param
        let id: any = req?.query?.id;

        // Obtaining data by Body
        let name: any = req?.body?.name || "Default name";
        let description: any = req?.body?.description || "Default description";
        let level: KataLevel = req?.body?.level || KataLevel.BASIC;
        let user: string = req?.body?.user || "";
        let dateString: any = req?.body?.date;
        let dateObj: Date = new Date(dateString ?? new Date());
        let valoration: number = req?.body?.valoration || 0;
        let numvaloration: any = req?.body?.numvaloration;
        let chances: number = req?.body?.chances;
        let solution: string = req?.body?.solution || "";
        let participants: string[] = req?.body?.participants || [];

        if(name && user){
            let Kata: IKata = {
                name: name,
                description: description || "Default description",
                level: level || "Basic",
                user: user,
                date: dateObj,
                valoration: valoration,
                numvaloration: numvaloration,
                chances: chances,
                solution: solution,
                participants: participants,
            }
            // Controller Instance to execute method
            const controller: KatasController = new KatasController();
            // Obtain Response
            const response: any = await controller.updateKata(id,Kata);
            // Send to the client the response
            return res.status(200).send(response);
        }else{
            return res.status(400).send({
                message:'[ERROR] Updating Kata, you ned to send name and user',
            });
        }

    })

// GET -> http://localhost:8000/api/katas/5last
kataRouter.route('/updatevaloration')
    //PUT:
    .put(async (req: Request, res: Response) => {
        // Obtain a Query Param
        let id: any = req?.query?.id;
        LogInfo(`Query Param: ${id}`);
        
        // creating a user in a object

        let valoration: any = req?.query?.valoration || "0";

        // Controller Instance to execute method
        const controller: KatasController = new KatasController();
        // Obtain Response
        const response: any = await controller.updateKataValoration(id, valoration);
        // Send to the client the response
        return res.send(response);
    })

// GET -> http://localhost:8000/api/katas/5last
kataRouter.route('/5last')
    // GET:
    .get(async (req: Request, res: Response) => {
        // Obtain a Query Param
        
        // Controller Instance to execute method
        const controller: KatasController = new KatasController();
        // Obtain Response
        const response: any = await controller.getKatasLast5();
        // Send to the client the response
        return res.send(response);

    })

// GET -> http://localhost:8000/api/katas/cop
kataRouter.route('/top')
    // GET:
    .get(async (req: Request, res: Response) => {
        // Obtain a Query Param
        
        // Controller Instance to execute method
        const controller: KatasController = new KatasController();
        // Obtain Response
        const response: any = await controller.getKatasTop();
        // Send to the client the response
        return res.send(response);

    })

// GET -> http://localhost:8000/api/katas/chances
kataRouter.route('/chances')
    // GET:
    .get(async (req: Request, res: Response) => {
        // Obtain a Query Param
        
        // Controller Instance to execute method
        const controller: KatasController = new KatasController();
        // Obtain Response
        const response: any = await controller.getKatasChances();
        // Send to the client the response
        return res.send(response);

    })


// Export Kata Router
export default kataRouter;