import express, { Request, Response } from "express";
import { AuthController } from "../controller/AuthController";
import { LogInfo } from "../utils/logger";
import { IUser } from "../domain/interfaces/IUser.interface";
import { IAuth } from "../domain/interfaces/IAuth.interface";

// BCRYPT for passwords
import bcrypt from 'bcrypt';

// JWT Verify MiddleWare 
import { verifyToken } from '../middlewares/verifyToken.middleware';

// Router from express
let authRouter = express.Router();

authRouter.route('/register')
    .post ( async (req:Request, res: Response) => {

        let { name, email, password, age } = req?.body;
        let hashedPassword = '';

        if(name && email && password && age){
            
            //Obtainint the password in request and crypher
            hashedPassword = bcrypt.hashSync(req.body.password, 8);

            let newUser: IUser = {
                name: name,
                email: email,
                password: hashedPassword,
                age: age,
                katas: []
            }

            //Controller Instance to execute method
            const controller: AuthController = new AuthController();

            // Obtain Response
            const response: any = await controller.registerUser(newUser);
            // Send to the client the response
            return res.status(response.status).send(response.message);

        }else {
            // Send to the client the response
            return res.status(400).send({
                message: '[ERROR User Data missing]: No user can be registered'
            })
        }
    })


authRouter.route('/login')
    .post ( async (req:Request, res: Response) => {

        let { email, password } = req?.body;
        if(email && password){

            //Controller Instance to execute method
            const controller: AuthController = new AuthController();

            let auth: IAuth = {
                email: email,
                password: password
            }

            // Obtain Response
            const response: any = await controller.loginUser(auth);
            // Send to the client the response which includes the JWT to authorize requests
            return res.status(response.status).send(response);

        }else {
            // Send to the client the response
            return res.status(400).send({
                message: '[ERROR User Data missing]: No user can be registered'
            })
        }
    })


// Route Protected by VERYFY TOKEN Middleware
authRouter.route('/me')
    .get(verifyToken, async (req: Request, res: Response) => {

        // Obtener el ID of User to check it's data
        let id: any = req?.query?.id;

        if(id){

            // Controller: Auth Controller
            const controller: AuthController = new AuthController();

            // Obtain response from Controller
            let response: any = await controller.userData(id);

            // If user is authorised:
            return res.status(200).send(response);
        }else{
            return res.status(401).send({
                message: 'You are not authorised to perform this action'
            })
        }



    })

// Export Auth Router
export default authRouter;