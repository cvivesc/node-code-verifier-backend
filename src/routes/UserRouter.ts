import express, { Request, Response } from "express";
import { UsersController } from "../controller/UsersController";
import { LogInfo } from "../utils/logger";

// JWT Verify MiddleWare 
import { verifyToken } from '../middlewares/verifyToken.middleware';

// Router from express
let userRouter = express.Router();

// GET -> http://localhost:8000/api/users?id=63fcfcd8260f9bb3ae348c1e
userRouter.route('/')
    // GET:
    .get(verifyToken, async (req: Request, res: Response) => {
        // Obtain a Query Param
        let id: any = req?.query?.id;
        // Pagination
        let page: any = req?.query?.page || 1;
        let limit: any = req?.query?.limit || 10;


        LogInfo(`Query Param: ${id}`);
        // Controller Instance to execute method
        const controller: UsersController = new UsersController();
        // Obtain Response
        const response: any = await controller.getUsers(page, limit, id);
        // Send to the client the response
        return res.status(200).send(response);

    })
    // DELETE:
    .delete(verifyToken, async (req: Request, res: Response) => {
        // Obtain a Query Param
        let id: any = req?.query?.id;
        LogInfo(`Query Param: ${id}`);
        // Controller Instance to execute method
        const controller: UsersController = new UsersController();
        // Obtain Response
        const response: any = await controller.deleteUser(id);
        // Send to the client the response
        return res.status(response.status).send(response.message);

    })
    // // POST:
    // .post(async (req: Request, res: Response) => {
    //     // creating a user in a object

    //     let name: any = req?.query?.name;
    //     let email: any = req?.query?.email;
    //     let age: any = req?.query?.age;

    //     //let name2: any = req?.body?.name;
    //     //LogInfo(`### NAME in BODY: ${name2}`);

    //     let user = {
    //         name: name || "Default name",
    //         email: email || "default@email.cat",
    //         age: age || 1
    //     }
    //     // Controller Instance to execute method
    //     const controller: UsersController = new UsersController();
    //     // Obtain Response
    //     const response: any = await controller.createUser(user);
    //     // Send to the client the response
    //     return res.status(200).send(response);

    // })

    //PUT:
    .put(verifyToken, async (req: Request, res: Response) => {
        // Obtain a Query Param
        let id: any = req?.query?.id;
        LogInfo(`Query Param: ${id}`);
        
        // creating a user in a object

        let name: any = req?.query?.name;
        let email: any = req?.query?.email;
        let age: any = req?.query?.age;

        let user = {
            name: name,
            email: email,
            age: age
        }

        // Controller Instance to execute method
        const controller: UsersController = new UsersController();
        // Obtain Response
        const response: any = await controller.updateUser(id, user);
        // Send to the client the response
        return res.status(response.status).send(response.message);
    });

// GET -> http://localhost:8000/api/users/katas?id=63fcfcd8260f9bb3ae348c1e
userRouter.route('/katas')
    // GET:
    .get(verifyToken, async (req: Request, res: Response) => {
        // Obtain a Query Param
        let id: any = req?.query?.id;
        // Pagination
        let page: any = req?.query?.page || 1;
        let limit: any = req?.query?.limit || 10;


        LogInfo(`Query Param: ${id}`);
        // Controller Instance to execute method
        const controller: UsersController = new UsersController();
        // Obtain Response
        const response: any = await controller.getKatas(page, limit, id);
        // Send to the client the response
        return res.status(200).send(response);
    });

// Export User Router
export default userRouter;