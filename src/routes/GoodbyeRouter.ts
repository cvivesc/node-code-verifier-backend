import { GoodbyeResponse } from "@/controller/types";
import express, { Request, Response } from "express";
import { GoodbyeController } from "../controller/GoodbyeController";
import { LogInfo } from "../utils/logger";

// Router from express
let goodbyeRouter = express.Router();

// GET -> http://localhost:8000/api/goodbyeRouter?name=Carles
goodbyeRouter.route('/')
    // GET:
    .get(async (req: Request, res: Response) => {
        // Obtain a Query Param
        let name: any = req?.query?.name;
        LogInfo(`Query Param: ${name}`);
        // Controller Instance to execute method
        const controller: GoodbyeController = new GoodbyeController();
        // Obtain Response
        const response: GoodbyeResponse = await controller.getMessage(name);
        // Send to the client the response
        return res.send(response);

    })

// Export Hello Router
export default goodbyeRouter;