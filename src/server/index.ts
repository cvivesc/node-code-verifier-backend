import express, { Express, Request, Response} from "express";
import mongoose from "mongoose";

// Swagger
import swaggerUi from 'swagger-ui-express';

// Security
import cors from 'cors';
import helmet from 'helmet';

// TODO: https

// Root Router
import rootRoutes from '../routes';

// * SCreate Express APP
const server: Express = express();

// * Swagger Config  and route
server.use(
    '/docs',
    swaggerUi.serve,
    swaggerUi.setup(undefined, {
        swaggerOptions: {
            url: "/swagger.json",
            explorer: true
        }
    })

)

// * Define server to use "/api" and use rootRoute from 'index.js' in routes
// From this point onover: http://localhost:8000/api/...
server.use(
    '/api',
    rootRoutes
);

// Static server
server.use(express.static('public'));

// TODO: Mongoose connection
mongoose.set('strictQuery', true);
mongoose.connect('mongodb://localhost:27017/codeverification')

// * Securiti Config
server.use(helmet());
server.use(cors());

// Content Type Config
server.use(express.urlencoded({ extended: true, limit: '50mb'}));
//server.use(express.json({limit: '50mb'}));
server.use(express.json());

// * Redirection Config
// http://localhost:8000/ --> http://localhost:8000/api/
server.get('/', (req: Request, res: Response) => {
    res.redirect('/api');
});

export default server;