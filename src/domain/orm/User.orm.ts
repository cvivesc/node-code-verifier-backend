import { userEntity } from "../entities/User.entity";

import { LogError, LogSuccess } from "../../utils/logger";
import { IUser } from "../interfaces/IUser.interface";
import { IAuth } from "../interfaces/IAuth.interface";

//Configuration the .env file
import dotenv from 'dotenv';
dotenv.config();
const secret = process.env.SECRETKEY || 'SECRETTEXT';


// BCRYPT from passwords
import bcrypt from 'bcrypt';
// JWT
import jwt from 'jsonwebtoken';
import { UserResponse } from "../types/UsersResponse.types";
import { kataEntity } from "../entities/Kata.Entity";
import { IKata } from "../interfaces/IKata.interface";

// CRUD

/**
 * Method to obtain all users from Collection "Users" in Mongo Server
 */
export const getAllUsers = async(page:number, limit: number) => {
    try {
        let userModel = userEntity();

        let response: any = {};

        //Search all users (using pagination)
        await userModel.find({isDelete: false},{password: 0, __v: 0})
            .limit(limit)
            .skip((page -1) * limit)
            .exec().then((users: IUser[]) => {
                response.users = users;
            });

        // Count total documents in collection Users
        await userModel.countDocuments().then((total: number) => {
            response.totalPages = Math.ceil(total / limit);
            response.currentPage = page;
        });

        return response;

        //return await userModel.find({isDelete: false},{password: 0, __v: 0})
    } catch (error) {
        LogError(`[ORM ERROR]: Getting All Users: ${error}`)
    }

}

// - Get User By ID
export const getUserByID = async(id: string): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        //Search user by ID
        return await userModel.findById(id,{password: 0, __v: 0});
    } catch (error) {
        LogError(`[ORM ERROR]: Getting User by ID: ${error}`);
    }

}

// - Delete User By ID
export const deleteUserByID = async(id: string): Promise<any> => {
    try {
        let userModel = userEntity();

        //Delete user by ID
        return await userModel.deleteOne({"_id":id});
        //return await userModel.findByIdAndDelete(id);
    } catch (error) {
        LogError(`[ORM ERROR]: Deleting User by ID: ${error}`);
    }
}

// - Update User By ID
export const updateUserByID = async(id: string, user: any): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        //return await userModel.findOneAndUpdate({"_id":id}, user);
        //return await userModel.updateOne({"_id":id}, user);
        return await userModel.findByIdAndUpdate(id, user);
    } catch (error) {
        LogError(`[ORM ERROR]: Updating User ${id}: ${error}`);
    }
}

// Register User
export const registerUser = async(user: IUser): Promise<any | undefined> => {
    try {

        let userModel = userEntity();

        // Create new user
        return await userModel.create(user);
    } catch (error) {
        LogError(`[ORM ERROR]: Registring User: ${error}`);
    }
    
}

// Login User
export const loginUser = async(auth: IAuth): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        let userFound: IUser | undefined = undefined;
        let token = undefined;


        // Check if User exists by Unique Email
        await userModel.findOne({ email: auth.email}).then((user: IUser) => {
            userFound = user;
        }).catch((error) => {
            console.error(`[ERROR Authentication in ORM]: User Not Found`);
            throw new Error(`[ERROR Authentication in ORM]: User Not Found: ${error}`);
        });


        // Check if Password is Valid (compare with bcrypt)
        let validPassword = bcrypt.compareSync(auth.password, userFound!.password);

        if(!validPassword) {
            console.error(`[ERROR Authentication in ORM]: Password Not Found`);
            throw new Error(`[ERROR Authentication in ORM]: Password Not Valid`);
        }
        
        // Generate JWT
        token = jwt.sign({ email: userFound!.email}, secret, {
            expiresIn: "2h"
        });

        return {
            user: userFound,
            token: token
        }
        //return await userModel.findByIdAndDelete(id);
    } catch (error) {
        LogError(`[ORM ERROR]: Deleting User by ID: ${error}`);
    }
    
}

// Logout User
export const logoutUser = async(): Promise<any | undefined> => {
    // TODO: Not implemented
    
}

/**
 * Method to obtain all users from Collection "Users" in Mongo Server
 */
 export const getKatasFromUser = async(page:number, limit: number, id: string) => {
    try {
        let userModel = userEntity();
        let kataModel = kataEntity();

        let response: any = {};

        await userModel.findById(id).then(async(user: IUser) => {
            //response.user.name = user.name;
            response.user = user.email;
            
            
            await kataModel.find({"_id": {"$in": user.katas}}).then((katas: IKata[]) => {
                response.katas = katas;
                console.log("KATAS:",response.katas);
            })
        }).catch((error) => {
            LogError(`[ORM ERROR]: Obtaining User: ${error}`);
        })

        return response;

        //return await userModel.find({isDelete: false},{password: 0, __v: 0})
    } catch (error) {
        LogError(`[ORM ERROR]: Getting All Users: ${error}`)
    }

}

