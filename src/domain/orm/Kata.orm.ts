import { kataEntity } from "../entities/Kata.Entity";

import { LogError, LogSuccess } from "../../utils/logger";
import { IKata } from "../interfaces/IKata.interface";

// GRUD

/**
 * Method to obtain all katas from Collection "katas" in Mongo Server
 */
export const getAllKatas = async(page:number, limit: number) => {
    try {
        let kataModel = kataEntity();

        let response: any = {};

        //Search all katas (using pagination)
        await kataModel.find({isDelete: false},{__v: 0})
            .limit(limit)
            .skip((page -1) * limit)
            .exec().then((katas: IKata[]) => {
                response.katas = katas;
            });

        // Count total documents in collection katas
        await kataModel.countDocuments().then((total: number) => {
            response.totalPages = Math.ceil(total / limit);
            response.currentPage = page;
        });

        return response;


        //Search all katas
        //return await kataModel.find({isDelete: false})
    } catch (error) {
        LogError(`[ORM ERROR]: Getting All Katas: ${error}`)
    }

}

// - Get Kata By ID
export const getKataByID = async(id: string): Promise<any | undefined> => {
    try {
        let kataModel = kataEntity();

        //Search kata by ID
        return await kataModel.findById(id);
    } catch (error) {
        LogError(`[ORM ERROR]: Getting Kata by ID: ${error}`);
    }

 }

// - Get Last 5 Katas
export const getKatasLast5 = async(): Promise<any | undefined> => {
    try {
        let kataModel = kataEntity();

        //Search kata by ID
        return await kataModel.find({isDelete: false})
            .sort({ date: -1 })
            .limit(5);
    } catch (error) {
        LogError(`[ORM ERROR]: Getting Kata by ID: ${error}`);
    }

 }

// - Get Top valoration Katas
export const getKatasTop = async(): Promise<any | undefined> => {
    try {
        let kataModel = kataEntity();

        //Search katas sorted by valoration
        return await kataModel.find({isDelete: false})
            .sort({ valoration: -1 });
    } catch (error) {
        LogError(`[ORM ERROR]: Getting Kata by ID: ${error}`);
    }

 }

// - Get Katas sorted by chances
export const getKatasChances = async(): Promise<any | undefined> => {
    try {
        let kataModel = kataEntity();

        //Search katas sorted by valoration
        return await kataModel.find({isDelete: false})
            .sort({ chances: -1 });
    } catch (error) {
        LogError(`[ORM ERROR]: Getting Kata by ID: ${error}`);
    }

 }

 // - Get Kata Filtered by Level
 export const getKataByLevel = async(level: string): Promise<any | undefined> => {
    try {
        let kataModel = kataEntity();

        //Search kata by ID
        return await kataModel.find({ "level": level });
    } catch (error) {
        LogError(`[ORM ERROR]: Getting Kata by LEVEL: ${error}`);
    }

 }

// - Delete User By ID
export const deleteKataByID = async(id: string, userID: string): Promise<any> => {
    try {
        let kataModel = kataEntity();

        // Verify user kata is the same of userID passed by post
        await kataModel.findById(id).then(async(kata:IKata) => {
            if(kata.user=userID){
                return await kataModel.deleteOne({"_id":id});
            }else{
                LogError(`[ORM ERROR]: User hasn't permissions to delete Kata by ID: ${id}`);
            }

        });
        // Delete user by ID
    } catch (error) {
        LogError(`[ORM ERROR]: Deleting Kata by ID: ${error}`);
    }
}

// - Create New Kata
export const createKata = async(kata: IKata): Promise<any | undefined> => {
    try {
        let kataModel = kataEntity();

        // Create new kata
        return await kataModel.create(kata);
    } catch (error) {
        LogError(`[ORM ERROR]: Creating Kata: ${error}`);
    }
}

// - Update Kata By ID
export const updateKataByID = async(id: string, kata: IKata): Promise<any | undefined> => {
    try {
        let kataModel = kataEntity();

        // Verify user kata is the same of userID passed by post
        await kataModel.findById(id).then(async(kataToUpdate:IKata) => {
            if(kataToUpdate.user==kata.user){
                return await kataModel.findByIdAndUpdate(id, kata);
            }else{
                LogError(`[ORM ERROR]: User hasn't permissions to update Kata by ID: ${id}`);
            }

        });
    } catch (error) {
        LogError(`[ORM ERROR]: Updating Kata ${id}: ${error}`);
    }
}

// - Update kata valoration By ID
export const updateKataValoration = async(id: string, valoration: any): Promise<any | undefined> => {
    try {
        let kataModel = kataEntity();

        //return await userModel.findOneAndUpdate({"_id":id}, user);
        //return await userModel.updateOne({"_id":id}, user);
        let kataValoration: any = await kataModel.find({'_id':id});
        let kataNumValoration: number = kataValoration[0].numvaloration;
        let kataValValoration: number = kataValoration[0].valoration;
        let kataNewvaloration: number = ((kataValValoration * kataNumValoration) + parseFloat(valoration)) / (kataNumValoration + 1);
        let kataUpdate = {
            valoration: kataNewvaloration,
            numvaloration: (kataNumValoration + 1)
        }
        LogSuccess(`newval:${valoration} | oldval:${kataValValoration} | numval:${kataNumValoration} | totalval:${kataNewvaloration}--`);
        return await kataModel.findByIdAndUpdate(id, kataUpdate);
    } catch (error) {
        LogError(`[ORM ERROR]: Updating Kata ${id}: ${error}`);
    }
}

// TODO:

// - Get User By Email

