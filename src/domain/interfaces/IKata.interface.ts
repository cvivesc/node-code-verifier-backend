export enum KataLevel {
    BASIC = 'Basic',
    MEDIUM = 'Medium',
    HIGHT = 'Hight'
}

export interface IKata {
    name: String,
    description: String,
    level: KataLevel,
    user: string, // Id of user creator
    date: Date,
    valoration: Number, // stars
    numvaloration: Number,
    chances: Number, // intents
    solution: string,
    participants: string[]
}