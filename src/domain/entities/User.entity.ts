import mongoose from "mongoose";
import { IUser } from "../interfaces/IUser.interface";

export const userEntity = () => {

    let userSchema = new mongoose.Schema<IUser>(
        {
            name: { type: String, require: true },
            email: { type: String, require: true },
            password: { type: String, require: true },
            age: { type: Number, require: true },
            katas: { type: [], require: true }
        }
    );

    return mongoose.models.users || mongoose.model<IUser>('users', userSchema);
    
}