import mongoose from "mongoose";
import { IKata } from "../interfaces/IKata.interface";

export const kataEntity = () => {

    let kataSquema = new mongoose.Schema<IKata>({
        name: { type: String, require: true },
        description: { type: String, require: true },
        level: { type: String, require: true },
        user: { type: String, require: true },
        date: { type: Date, require: true },
        valoration: { type: Number, require: true },
        numvaloration: { type: Number, require: true },
        chances: { type: Number, require: true },
        solution: { type: String, require: true },
        participants: { type: [], require: true },
    })

    return mongoose.models.katas || mongoose.model<IKata>('katas', kataSquema);
    
}